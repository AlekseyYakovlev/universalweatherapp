package ru.spb.yakovlev.universalweatherapp.UI;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import ru.spb.yakovlev.universalweatherapp.R;
import ru.spb.yakovlev.universalweatherapp.UI.CitiesScreen.CitiesFragment;
import ru.spb.yakovlev.universalweatherapp.UI.WeatherScreen.WeatherFragment;
import ru.spb.yakovlev.universalweatherapp.dummy.DummyContent;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener
        , CitiesFragment.OnListFragmentInteractionListener {

    private DrawerLayout drawer;

    private DummyContent.DummyItem activeItem;
    private boolean delTrigger;
    private ActionMode citiesFragmentActionMode;
    public ActionMode.Callback citiesFragmentActionModeCallback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.main, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {

            int id = item.getItemId();


            if (id == R.id.action_add) {
                Log.d("", "add");
                mode.finish();
                return true;
            }
            if (id == R.id.action_delete) {
                Log.d("", "del " + activeItem.cityName);
                DummyContent.ITEMS.remove(activeItem);
                delTrigger = true;
                mode.finish();
                activeItem = null;
                return true;
            }
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            citiesFragmentActionMode = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initNavDrawer(toolbar);

        showWeatherFragment();


    }

    private void initNavDrawer(Toolbar toolbar) {
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }


    private void showCitiesFragment() {
        CitiesFragment citiesFragment = new CitiesFragment();
        inflateFragment(citiesFragment);
    }

    private void showWeatherFragment() {
        WeatherFragment weatherFragment = new WeatherFragment();
        inflateFragment(weatherFragment);
    }


    private void inflateFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.main_fragment_container, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.weather_screen) {
            showWeatherFragment();
        } else if (id == R.id.cities_screen) {
            showCitiesFragment();
        } else if (id == R.id.settings_screen) {

        } else if (id == R.id.themes_screen) {

        } else if (id == R.id.about_screen) {
            showAboutDialog();
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showAboutDialog() {
        new AlertDialog.Builder(this)
                .setTitle(getText(R.string.about_screen_title))
                .setMessage(getText(R.string.about_screen_text))
                //.setIcon()
                .setCancelable(false)
                .setNegativeButton(getText(R.string.ok),
                        (dialog, id) -> dialog.cancel())
                .create()
                .show();
    }

    @Override
    public boolean onItemSelected(DummyContent.DummyItem item) {
        Log.d("rr", item.cityName);
        if (citiesFragmentActionMode != null) {
            return false;
        }
        delTrigger = false;
        activeItem = item;
        citiesFragmentActionMode = startSupportActionMode(citiesFragmentActionModeCallback);
        //view.setSelected(true);
        return delTrigger;
    }

    @Override
    public void onItemLongClicked(DummyContent.DummyItem item) {

    }


}
