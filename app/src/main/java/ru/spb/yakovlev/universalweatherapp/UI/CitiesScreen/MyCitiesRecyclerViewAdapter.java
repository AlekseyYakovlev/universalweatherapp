package ru.spb.yakovlev.universalweatherapp.UI.CitiesScreen;

import android.content.res.Resources;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import ru.spb.yakovlev.universalweatherapp.R;
import ru.spb.yakovlev.universalweatherapp.dummy.DummyContent.DummyItem;


public class MyCitiesRecyclerViewAdapter
        extends RecyclerView.Adapter<MyCitiesRecyclerViewAdapter.ViewHolder> {
    private final List<DummyItem> mValues;
    private final CitiesFragment.OnListFragmentInteractionListener mListener;
    private String scaleName = "C"; //TODO Make updatable from settings
    private int selectedPos = RecyclerView.NO_POSITION;

    public MyCitiesRecyclerViewAdapter(List<DummyItem> items, CitiesFragment.OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_cities, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.itemView.setSelected(selectedPos == position);

        DummyItem mItem = holder.mItem = mValues.get(position);

        Resources resources = holder.mView.getResources();

        holder.tvCityName.setText(holder.mItem.cityName);
        holder.tvTemp.setText(String.format(resources.getString(R.string.tv_temp), holder.mItem.temp, scaleName));
        holder.tvIcon.setText(mValues.get(position).icon);
        holder.tvLastUpdated.setText(String.format(resources.getString(R.string.tv_last_updated), getFormattedDate(holder.mItem.lastUpdateDate)));
        holder.tvSource.setText(String.format(resources.getString(R.string.tv_source), holder.mItem.dataSource));

        holder.mView.setOnClickListener(v -> {
            if (null != mListener) {
                // Notify the active callbacks interface (the activity, if the
                // fragment is attached to one) that an item has been selected.

                //FIXME: Fix selection
//                notifyItemChanged(selectedPos);
//                selectedPos = position;

//                v.setSelected(!v.isSelected());
//                int backgroundColor = v.isSelected() ? R.color.colorAccent : R.color.lightGray;
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                    v.setBackgroundColor(v.getResources().getColor(backgroundColor, null));
//                } else v.setBackgroundColor(v.getResources().getColor(backgroundColor));

                if (mListener.onItemSelected(holder.mItem)) notifyItemRemoved(position);
                else notifyItemChanged(position);

                //holder.setClicked();
            }
        });


        holder.mView.setOnCreateContextMenuListener((menu, v, menuInfo) -> {
            menu.setHeaderTitle(resources.getString(R.string.menu_group_select_action));

            menu
                    .add(R.string.menu_action_refresh)
                    .setOnMenuItemClickListener(item -> {
                        Snackbar
                                .make(holder.mView, "Refreshing element", Snackbar.LENGTH_LONG)
                                .show();
                        // refreshCode
                        notifyItemChanged(position);
                        return true;
                    });

            menu
                    .add(R.string.menu_action_delete)
                    .setOnMenuItemClickListener(item -> {
                        deleteCity(holder.mView, position, mItem);
                        return true;
                    });
        });

    }

    public void deleteCity(View view, int position, DummyItem mItem) {
        String actionRequestString =
                String.format(view.getResources().getString(R.string.menu_delete_action_request), mItem.cityName);

        Snackbar
                .make(view, actionRequestString, Snackbar.LENGTH_LONG)
                .setAction(R.string.yes, v1 -> {
                    notifyItemRemoved(position);
                    mValues.remove(mItem);
                })
                .show();
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    String getFormattedDate(long date) {
        return new SimpleDateFormat("dd MMMM YYYY hh:mm:ss",
                new Locale("ru", "RU")).format(date);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final TextView tvCityName;
        final TextView tvTemp;
        final TextView tvIcon;
        final TextView tvLastUpdated;
        final TextView tvSource;
        DummyItem mItem;
//        private boolean isSelected;

        ViewHolder(View view) {
            super(view);
            mView = view;
            tvCityName = view.findViewById(R.id.cities_fr_tv_city);
            tvTemp = view.findViewById(R.id.cities_fr_tv_temperature);
            tvIcon = view.findViewById(R.id.cities_fr_tv_ic);
            tvLastUpdated = view.findViewById(R.id.cities_fr_tv_last_updated);
            tvSource = view.findViewById(R.id.cities_fr_tv_source);

        }

//        void setClicked() {
//            isSelected = !isSelected;
//            int backgroundColor = isSelected ? R.color.colorAccent : R.color.lightGray;
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                mView.setBackgroundColor(mView.getResources().getColor(backgroundColor, null));
//            } else mView.setBackgroundColor(mView.getResources().getColor(backgroundColor));
//        }
    }


}
