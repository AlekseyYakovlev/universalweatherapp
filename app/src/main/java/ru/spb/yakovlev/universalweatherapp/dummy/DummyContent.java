package ru.spb.yakovlev.universalweatherapp.dummy;

import android.os.Build;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class DummyContent {

    /**
     * An array of sample (dummy) items.
     */
    public static final List<DummyItem> ITEMS = new ArrayList<>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static final Map<String, DummyItem> ITEM_MAP = new HashMap<>();

    private static final int COUNT = 25;

    static {
        // Add some sample items.
        for (int i = 1; i <= COUNT; i++) {
            addItem(createDummyItem(i));
        }
    }

    private static void addItem(DummyItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    private static void deleteItem(DummyItem item) {
        ITEMS.remove(item);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            ITEM_MAP.remove(item.id, item);
        } else ITEM_MAP.remove(item.id);
    }

    private static DummyItem createDummyItem(int position) {
        return new DummyItem(String.valueOf(position)
                , "City name  " + position
                , -15.6f
                , "" //cloud+sun
                , System.currentTimeMillis()
                , "GisMeteo"
        );
    }


    /**
     * A dummy item representing a piece of content.
     */
    public static class DummyItem {
        public final String id;
        public final String cityName;
        public final float temp;
        public final String icon;
        public final long lastUpdateDate;
        public final String dataSource;

        public DummyItem(String id, String cityName, float temp, String icon, long lastUpdateDate, String dataSource) {
            this.id = id;
            this.cityName = cityName;
            this.temp = temp;
            this.icon = icon;
            this.lastUpdateDate = lastUpdateDate;
            this.dataSource = dataSource;
        }
    }
}
