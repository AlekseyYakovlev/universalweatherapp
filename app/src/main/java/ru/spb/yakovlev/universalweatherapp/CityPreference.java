package ru.spb.yakovlev.universalweatherapp;

import android.app.Activity;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;

/**
 * Created by Aleksey on 04.03.2018.
 */

public class CityPreference {
    private static final String KEY = "city";
    private static final String DEFAULT = "Saint Petersburg";
    private SharedPreferences userPreferences;

    public CityPreference(Fragment fragment) {
        userPreferences = fragment.getActivity().getPreferences(Activity.MODE_PRIVATE);
    }

    public String getCity() {
        return userPreferences.getString(KEY, DEFAULT);
    }

    public void setCity(String city) {
        userPreferences.edit().putString(KEY, city).apply();
    }
}